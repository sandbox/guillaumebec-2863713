<?php

namespace Drupal\html2entity\Element;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\CompositeFormElementTrait;
use Drupal\Core\Render\Element\FormElement;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides an element to manage ImportSchema components.
 *
 * Properties:
 * - #entity_type: The target entity type.
 * - #entity_bundle: The target entity bundle.
 * - #components: (optional) Existing values.
 * - #title: (optional) Enforces the title of the components list.
 *
 * Usage example:
 * @code
 * $build['components'] = [
 *   '#type' => 'import_schema_components',
 *   '#entity_type' => 'node',
 *   '#entity_bundle' => 'article',
 *   '#components' => ['title' => '#mydiv'],
 * ];
 * @endcode
 *
 * @FormElement("import_schema_components")
 */
class ImportSchemaComponents extends FormElement {

  use CompositeFormElementTrait;

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected static $fieldManager;

  /**
   * The import schema component plugin manager.
   *
   * @var \Drupal\html2entity\Plugin\ImportSchemaComponentManager
   */
  protected static $componentManager;

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#title' => $this->t('Components'),
      '#tree' => TRUE,
      '#components' => [],
      '#process' => [
        [$class, 'processComponents'],
      ],
    ];
  }

  /**
   * Render API callback: Expands the components element type.
   */
  public static function processComponents(&$element, FormStateInterface $form_state, &$complete_form) {
    if (empty($element['#entity_type']) || empty($element['#entity_bundle'])) {
      return [];
    }
    $entity_type = $element['#entity_type'];
    $entity_bundle = $element['#entity_bundle'];

    // Get user input to react to Ajax calls.
    $input = $form_state->getUserInput();
    $element_input = NestedArray::getValue($input, $element['#array_parents']);

    // Prepare components.
    // @see ::valueCallback().
    $components = $element['#value'];

    // Generate a unique wrapper HTML ID.
    $ajax_wrapper_id = Html::getUniqueId('ajax-wrapper');

    // Prefix and suffix used for Ajax replacement.
    $element['#tree'] = TRUE;
    $element['#prefix'] = '<div id="' . $ajax_wrapper_id . '">';
    $element['#suffix'] = '</div>';

    // List of existing components.
    $element['_components'] = [
      '#type' => 'container',
      '#title' => $element['#title'],
      '#pre_render' => [[get_called_class(), 'preRenderCompositeFormElement']],
    ];
    if (empty($components)) {
      $element['_components']['#markup'] = t('No component to show yet.');
    }
    else {
      $fields = self::getFieldManager()->getFieldDefinitions($entity_type, $entity_bundle);
      foreach ($components as $field_name => $values) {
        if (isset($fields[$field_name])) {
          $field_type = $fields[$field_name]->getType();
          $plugin = self::getComponentsManager()->createInstance($field_type);

          $component = [
            '#parents' => $element['#parents'],
            '#array_parents' => $element['#array_parents'],
            '#default_value' => $values,
          ];
          $component['#parents'][] = $field_name;
          $component['#array_parents'][] = '_components';
          $component['#array_parents'][] = $field_name;
          $component += $plugin->formElement($component, $element, $form_state, $fields[$field_name]);
          $element['_components'][$field_name] = $component;
        }
      }
    }

    // Add a new component.
    $element['_add_component'] = [
      '#type' => 'container',
      '#title' => t('Add a component'),
      '#pre_render' => [[get_called_class(), 'preRenderCompositeFormElement']],
      '#element_validate' => [[get_called_class(), 'removeElementValue']],
    ];
    $element['_add_component']['field'] = [
      '#type' => 'select',
      '#title' => t('Choose a field'),
      '#title_display' => 'invisible',
      '#empty_option' => t('- Choose a field -'),
      '#options' => self::getFieldsOptions($entity_type, $entity_bundle, array_keys($components)),
      // Use the previous value as empty value to avoid validation errors.
      '#empty_value' => !empty($element_input['_add_component']['field']) ? $element_input['_add_component']['field'] : '',
    ];
    $element['_add_component']['button'] = [
      '#type' => 'button',
      '#value' => t('Create the component'),
      '#limit_validation_errors' => [],
      '#ajax' => [
        'callback' => [get_called_class(), 'createComponent'],
        'options' => ['query' => ['element_parents' => implode('/', $element['#array_parents'])]],
        'wrapper' => $ajax_wrapper_id,
      ],
    ];
    if (empty($element['_add_component']['field']['#options'])) {
      $element['_add_component']['field']['#empty_option'] = t('- No more available field -');
      $element['_add_component']['field']['#disabled'] = TRUE;
      $element['_add_component']['button']['#disabled'] = TRUE;
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    $components = $element['#components'];

    if (!empty($input)) {
      $components = array_merge($components, $input);
    }

    $element_values = $form_state->getValue($element['#array_parents']);
    if (!empty($element_values)) {
      $components = array_merge($components, $element_values);
    }

    // Add the new component.
    if (!empty($input['_add_component']['field']) && !array_key_exists($input['_add_component']['field'], $components)) {
      $components[$input['_add_component']['field']] = '';
    }

    unset($components['_add_component']);
    return $components;
  }

  /**
   * Removes the element value from the form_state.
   *
   * Used to avoid technical elements to pollute the final values.
   *
   * @param array $element
   *   The element to remove from the values.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function removeElementValue(&$element, FormStateInterface $form_state) {
    $form_state->unsetValue($element['#parents']);
  }

  // ---------------------------------------------------------------------------
  // Helpers.

  /**
   * Gets the array of available fields to be used as options in a select.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   * @param array $used
   *   The machine name of the already used fields.
   *
   * @return array
   *   The fields to show in the select box.
   */
  protected static function getFieldsOptions($entity_type, $bundle, array $used = []) {
    $options = [];
    $bundle_fields = self::getFieldManager()->getFieldDefinitions($entity_type, $bundle);
    foreach ($bundle_fields as $field) {
      // Use the field if it is not used yet, not computed and is writable.
      if (!in_array($field->getName(), $used) && !$field->isComputed() && !$field->isReadOnly()) {
        $arguments = [
          '@label' => $field->getLabel(),
          '@type' => $field->getType(),
        ];
        if ($field->getType() === 'entity_reference' || $field->getType() === 'entity_reference_revisions') {
          $arguments['@type'] .= ':' . $field->getSetting('target_type');
        }
        $options[$field->getName()] = new FormattableMarkup('@label (@type)', $arguments);
      }
    }
    uasort($options, 'strnatcasecmp');
    return $options;
  }

  // ---------------------------------------------------------------------------
  // Ajax callbacks.

  /**
   * Ajax callback to refresh add a field in the components.
   *
   * @param array $form
   *   The build form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return array
   *   The render array of the field to replace.
   */
  public static function createComponent(&$form, FormStateInterface &$form_state, Request $request) {
    $form_parents = explode('/', $request->query->get('element_parents'));

    // Retrieve the element to be rendered.
    $element = NestedArray::getValue($form, $form_parents);

    return $element;
  }

  // ---------------------------------------------------------------------------
  // Dependencies.

  /**
   * Gets the entity field manager service.
   *
   * @return \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected static function getFieldManager() {
    if (empty(self::$fieldManager)) {
      self::$fieldManager = \Drupal::service('entity_field.manager');
    }
    return self::$fieldManager;
  }

  /**
   * Gets the import schema component plugin manager.
   *
   * @return \Drupal\html2entity\Plugin\ImportSchemaComponentManager
   */
  protected static function getComponentsManager() {
    if (empty(self::$componentManager)) {
      self::$componentManager = \Drupal::service('plugin.manager.import_schema_component');
    }
    return self::$componentManager;
  }
}
