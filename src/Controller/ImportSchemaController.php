<?php

namespace Drupal\html2entity\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
/**
 * Provides methods for import schema routes.
 */
class ImportSchemaController extends ControllerBase {

  /**
   * Provides a list of eligible entity types for adding import schema.
   *
   * @return array
   *   A list of entity types to add a import schema for.
   */
  public function importSchemaTypeSelection() {
    $entity_types = array();
    foreach ($this->entityManager()->getDefinitions() as $entity_type_id => $entity_type) {
      // @TODO Test if has field that can be populate ?
      if ($entity_type->isSubclassOf('\Drupal\Core\Entity\FieldableEntityInterface'))
      $entity_types[$entity_type_id] = array(
        'title' => $entity_type->getLabel(),
        'url' => Url::fromRoute('entity.import_schema.add_form', array('entity_type_id' => $entity_type_id)),
        'localized_options' => array(),
      );
    }
    return array(
      '#theme' => 'admin_block_content',
      '#content' => $entity_types,
    );
  }

}
