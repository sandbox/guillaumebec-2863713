<?php

namespace Drupal\html2entity\Plugin\migrate\process;

use DateTime;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * This plugin convert a date to a timestamp using the given format or trying to
 * guess.
 *
 * @MigrateProcessPlugin(
 *   id = "strtotime"
 * )
 */
class StrToTime extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (!is_scalar($value)) {
      throw new MigrateException('Input should be a string.');
    }

    if (empty($this->configuration['format'])) {
      $time = strtotime($value);
    }
    else {
      $time = DateTime::createFromFormat($this->configuration['format'], $value);
      if ($time instanceof DateTime) {
        $time = $time->getTimestamp();
      }
    }

    if ($time === FALSE) {
      throw new MigrateException('The time cannot be extracted from the string.');
    }
    return $time;
  }

}
