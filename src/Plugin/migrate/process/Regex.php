<?php

namespace Drupal\html2entity\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * This plugin uses a regex to extract value from a string.
 *
 * @MigrateProcessPlugin(
 *   id = "regex"
 * )
 */
class Regex extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (!is_scalar($value)) {
      throw new MigrateException('Input should be a string.');
    }

    preg_match_all($this->configuration['pattern'], $value, $matches);

    if (empty($matches[1])) {
      throw new MigrateException('The input does not match the pattern.');
    }
    return $matches[1];
  }

}
