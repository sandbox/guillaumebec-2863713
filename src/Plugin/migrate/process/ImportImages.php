<?php

namespace Drupal\html2entity\Plugin\migrate\process;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\editor\Entity\Editor;
use Drupal\filter\Element\TextFormat;
use Drupal\filter\Entity\FilterFormat;
use Drupal\migrate\Plugin\MigrateProcessInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Extract images from an HTML string then import files and replace tags.
 *
 * @MigrateProcessPlugin(
 *   id = "html2entity_import_images"
 * )
 */
class ImportImages extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\file\FileStorageInterface
   */
  protected $fileStorage;

  /**
   * @var \Drupal\migrate\Plugin\MigrateProcessInterface
   */
  protected $fileCopyPlugin;

  /**
   * Class constructor.
   *
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   * @param \Drupal\migrate\Plugin\MigrateProcessInterface $file_copy_plugin
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, MigrateProcessInterface $file_copy_plugin) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->fileStorage = $entityTypeManager->getStorage('file');
    $this->fileCopyPlugin = $file_copy_plugin;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.migrate.process')->createInstance('file_copy', ['rename' => TRUE])
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $src_domain = $row->getSourceProperty('constants')['source_base_url'];
    $replacement_pattern = ' data-entity-type="file" data-entity-uuid=":uuid" src=":src"';
    $field_name = explode('/', $destination_property)[0];

    $format = $row->getSourceProperty($field_name . '_format')[0];
    $img_upload_settings = Editor::load($format)->getImageUploadSettings();
    $file_destination = $img_upload_settings['scheme'] . '://' . $img_upload_settings['directory'];

    // Extract img tags from the HTML string.
    preg_match_all('/<img.*?\/?>/', $value, $matches);

    // For each img tag.
    $replacements = [];
    foreach ($matches[0] as $img_tag) {
      // Extract the src attribute.
      preg_match('/src="(.*?)"/', $img_tag, $parts);

      // Avoid handling the same src twice.
      if (!array_key_exists($parts[0], $replacements)) {
        $url = $parts[1];

        // If the URL starts with http.
        if (strpos($url, 'http') === 0) {
          // But if it's not from the source domain.
          if (strpos($url, $src_domain) !== 0) {
            continue;
          }
        }
        // If the URL does not start with http, add the source domain.
        else {
          $url = $src_domain . '/' . ltrim($url, '/');
        }

        // Copy the file locally.
        $url = $this->fileCopyPlugin->transform([$url, $file_destination . '/' . basename($url)], $migrate_executable, $row, $destination_property);

        // Create the file entity.
        /* @var \Drupal\file\Entity\File $file */
        $file = $this->fileStorage->create([
          'uri' => $url,
          'uid' => $row->getSourceProperty('uid'),
        ]);
        $file->save();

        // Prepare the replacement.
        $replacements[$parts[0]] = strtr($replacement_pattern, [
          ':uuid' => $file->uuid(),
          ':src'  => file_url_transform_relative(file_create_url($file->getFileUri())),
        ]);
      }
    }

    return strtr($value, $replacements);
  }

}