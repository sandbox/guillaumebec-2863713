<?php

/**
 * @file
 * Contains \Drupal\html2entity\Plugin\Field\FieldType\ImporterFromUrlFieldType.
 */

namespace Drupal\html2entity\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\UriItem;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'importer_from_url_field_type' field type.
 *
 * @FieldType(
 *   id = "importer_from_url_field_type",
 *   label = @Translation("Importer from url field type"),
 *   description = @Translation("Add an url field that allow to import data when creating content."),
 *   default_widget = "importer_from_url_field_widget",
 *   default_formatter = "uri_link",
 * )
 */
class ImporterFromUrlFieldType extends UriItem {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);

    $properties['importer_schema_id'] = DataDefinition::create('string')
      ->setLabel(t('Importer schema'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);
    $schema['columns']['importer_schema_id'] = [
      'description' => 'The Importer schema machine name.',
      'type' => 'varchar',
      'length' => 100,
    ];

    return $schema;
  }
}
