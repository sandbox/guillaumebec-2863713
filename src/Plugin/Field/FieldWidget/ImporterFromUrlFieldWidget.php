<?php

/**
 * @file
 * Contains \Drupal\html2entity\Plugin\Field\FieldWidget\ImporterFromUrlFieldWidget.
 */

namespace Drupal\html2entity\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\UriWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\Query\QueryFactory;

/**
 * Plugin implementation of the 'importer_from_url_field_widget' widget.
 *
 * @FieldWidget(
 *   id = "importer_from_url_field_widget",
 *   label = @Translation("Importer from url field widget"),
 *   field_types = {
 *     "importer_from_url_field_type"
 *   }
 * )
 */
class ImporterFromUrlFieldWidget extends UriWidget implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Entity\Query\QueryFactory definition.
   *
   * @var Drupal\Core\Entity\Query\QueryFactory
   */
  protected $queryFactory;

  /**
   * Drupal\Core\Entity\Query\QueryFactory definition.
   *
   * @var Drupal\Core\Entity\Query\QueryFactory
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, QueryFactory $entity_query, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->queryFactory = $entity_query;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($plugin_id, $plugin_definition, $configuration['field_definition'], $configuration['settings'], $configuration['third_party_settings'], $container->get('entity.query'), $container->get('entity_type.manager'));
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $values = $form_state->getValues();
    if (!empty($values)) {

    }

    $element['value']['#title'] = $this->t('Url to import');
    $widget_entity_type = $this->fieldDefinition->getTargetEntityTypeId();
    $widget_bundle = $this->fieldDefinition->getTargetBundle();

    $ids = $this->queryFactory
      ->get('import_schema')
      ->condition('targetEntityType', $widget_entity_type)
      ->condition('targetBundles.' . $widget_bundle, $widget_bundle)
      ->execute();

    // @TODO: Can we disable it in the select so it can't be selected ?
    $options[] = '- No Importer for this -';
    if (!empty($ids)) {
      // Do we really need to do that to get the label of the entity ?
      $options = $this->entityTypeManager->getStorage('import_schema')->loadMultiple($ids);
      array_walk($options, function(&$value, $key) { $value = $value->label(); });
    }

    $element['importer_schema_id'] = array(
      '#type' => 'select',
      '#title' => $this->t('Importer schema'),
      '#default_value' => isset($items[$delta]->importer_schema_id) ? $items[$delta]->importer_schema_id : NULL,
      '#options' => $options,
      //'#element_validate' => array(array(get_called_class(), 'validateImportSchemaElement')),
      '#maxlength' => 100,
      '#required' => $element['#required'],
    );
    $element['import_and_refresh'] = array(
      '#type' => 'button',
      '#value' => $this->t('Import'),
      '#submit' => array(array($this, 'importCallback')), // Don't seems to be used with #type button.
      '#ajax' => [
        'callback' => array($this, 'refreshCallback'),
        'event' => 'click',
        'wrapper' => $form_state->getFormObject()->getFormId(),
        //'wrapper' => 'edit-field-importer-wrapper',
      ],
    );
    $element += array(
      '#type' => 'fieldset',
    );
    return $element;
  }

  /**
   * Implements callback for Ajax event to import html and pre-fill the form.
   *
   * @param array $form
   *   From render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current state of form.
   *
   * @return array
   */
  public function importCallback(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $form_state['rebuild'] = TRUE;
    return $form;
  }

  /**
   * Implements callback for Ajax event to import html and pre-fill the form.
   *
   * @param array $form
   *   From render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current state of form.
   *
   * @return array
   */
  public function refreshCallback(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    // Call the stuff.
    // Get the data
    $form['body']['widget'][0]['value']['#value'] = 'Body default value';
    $form['body']['widget'][0]['value']['#default_value'] = 'Body default value';
    $form['title']['widget'][0]['value']['#value'] = 'Title default value';
    $form['title']['widget'][0]['value']['#default_value'] = 'Title default value';
    $form['field_tags']['widget'][0]['target_id']['#value'] = 'Tags default value, and another one';
    //$form = array_merge_recursive($form, $data);
    //$form_state->setRebuild(TRUE);
    return $form;
  }
}

