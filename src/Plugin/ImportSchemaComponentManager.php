<?php

namespace Drupal\html2entity\Plugin;

use Drupal\Component\Plugin\FallbackPluginManagerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Import schema component plugin manager.
 */
class ImportSchemaComponentManager extends DefaultPluginManager implements FallbackPluginManagerInterface {

  /**
   * Constructor for ImportSchemaComponentManager objects.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/ImportSchemaComponent', $namespaces, $module_handler, 'Drupal\html2entity\Plugin\ImportSchemaComponentInterface', 'Drupal\html2entity\Annotation\ImportSchemaComponent');

    $this->alterInfo('html2entity_import_schema_component_info');
    $this->setCacheBackend($cache_backend, 'html2entity_import_schema_component_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function getFallbackPluginId($plugin_id, array $configuration = array()) {
    return 'default_component';
  }

}
