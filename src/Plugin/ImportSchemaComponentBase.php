<?php

namespace Drupal\html2entity\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Base class for Import schema component plugins.
 */
abstract class ImportSchemaComponentBase extends PluginBase implements ImportSchemaComponentInterface {

  use StringTranslationTrait;

  protected function getElementProperty(FieldDefinitionInterface $definition) {
    return ['value' => $this->t('Selector')];
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(array $element_infos, array $form, FormStateInterface $form_state, FieldDefinitionInterface $definition) {
    $value = $element_infos['#default_value'];
    $element = [
      '#type' => 'details',
      '#open' => 'TRUE',
      '#title' => $definition->getLabel(),
    ];

    foreach ($this->getElementProperty($definition) as $property => $label) {
      $element[$property] = [
        '#type' => 'container',
        '#attributes' => ['class' => ['container-inline']],
      ];
      $element[$property]['selector'] = [
        '#type' => 'textfield',
        '#title' => $label,
        '#maxlength' => 100,
        '#default_value' => (isset($value[$property]) ? $value[$property]['selector'] : ''),
      ];
      $element[$property]['data_source_type'] = [
        '#type' => 'select',
        '#options' => [
          'html' => $this->t('Html'),
          'attr' => $this->t('Attribute')
        ],
        '#title' => '',
        '#default_value' => (isset($value[$property]) ? $value[$property]['data_source_type'] : ''),
      ];
      $select_field_name = $form['#name'] . '[' . $definition->getName() . "][$property][data_source_type]";
      $element[$property]['data_source'] = [
        '#type' => 'textfield',
        '#maxlength' => 50,
        '#title' => '',
        '#default_value' => (isset($value[$property]) ? $value[$property]['data_source'] : ''),
        '#states' => [
          'visible' => [
            ':input[name="' . $select_field_name . '"]' => array('value' => 'attr'),
          ],
        ],
      ];
    }
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function getMigrationProcess($field_name, FieldDefinitionInterface $field_definition) {
    $process = [];
    $storage_definition = $field_definition->getFieldStorageDefinition();
    foreach ($this->getElementProperty($field_definition) as $property => $label) {
      // Avoid process treatment for property not used in the data source.
      $process[$field_name . '/' . $property][] = [
        'plugin' => 'skip_on_empty',
        'method' => 'process',
        'source' => $field_name . '_' . $property,
      ];

      // Basic step.
      $process_step = [
        //'source' => $field_name . '_' . $property,
      ];
      if ($storage_definition->isMultiple()){
        $process_step['plugin'] = 'get';
      }
      else {
        $process_step['plugin'] = 'extract';
        $process_step['index'] = [0];
      }
      $process[$field_name . '/' . $property][] = $process_step;
    }
    return $process;
  }

}
