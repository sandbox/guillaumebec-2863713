<?php

namespace Drupal\html2entity\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\html2entity\Entity\ImportSchema;

/**
 * Defines an interface for Import schema component plugins.
 */
interface ImportSchemaComponentInterface extends PluginInspectionInterface {

  /**
   * Creates a form element for a field.
   *
   * Display a form for field properties or others need.
   *
   * @param array $element_infos
   *   Information about the element that's going to be altered. It contains
   *   #parents, #array_parents and #default_value. These values cannot be overridden.
   * @param array $form
   *   An array representing the form that the editing element will be attached
   *   to.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $definition
   *   The field definition.
   *
   * @return array
   *   The form element array created for this field.
   */
  public function formElement(array $element_infos, array $form, FormStateInterface $form_state, FieldDefinitionInterface $definition);

  /**
   * Generates a migration process to import raw data into the field.
   *
   * @param string $field_name
   *   The field name, also used as source name.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   *
   * @return array
   * The migration processes array keyed by property name of the field.
   */
  public function getMigrationProcess($field_name, FieldDefinitionInterface $field_definition);
}
