<?php

namespace Drupal\html2entity\Plugin\ImportSchemaComponent;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\html2entity\Plugin\ImportSchemaComponentBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Plugin implementation of the 'entity_reference' component.
 *
 * @ImportSchemaComponent(
 *   id = "entity_reference",
 *   label = @Translation("Entity Reference component for ImportSchema field."),
 *   field_types = {
 *     "entity_reference"
 *   },
 * )
 */
class EntityReferenceComponent extends ImportSchemaComponentBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(array $element_infos, array $form, FormStateInterface $form_state, FieldDefinitionInterface $definition) {
    $value = $element_infos['#default_value'];
    $settings = $definition->getSettings();
    $element = [
      '#type' => 'details',
      '#open' => 'TRUE',
      '#title' => $definition->getLabel(),
    ];

    // Generate a unique wrapper HTML ID.
    $ajax_wrapper_id = Html::getUniqueId('ajax-wrapper');
    $element['#tree'] = TRUE;
    $element['#prefix'] = '<div id="' . $ajax_wrapper_id . '">';
    $element['#suffix'] = '</div>';
    $element['entity_components'] = [
      '#type' => 'container',
    ];

    $target_bundle = NULL;

    // @TODO should find a way to hide the add entity button when we already have
    // one, or we need to handle multiple entities.
    if (isset($settings['handler_settings']['target_bundles']) && count($settings['handler_settings']['target_bundles']) > 1) {
      // Add a new entity.
      $element['_add_entity'] = [
        '#type' => 'fieldset',
        '#title' => t('Add an entity'),
      ];
      $element['_add_entity']['bundle'] = [
        '#type' => 'select',
        '#title' => t('Choose a bundle'),
        '#title_display' => 'invisible',
        '#empty_option' => t('- Choose a bundle -'),
        //'#options' => self::getFieldsOptions($entity_type, $entity_bundle, array_keys($components)),
        '#options' => $settings['handler_settings']['target_bundles'],
      ];
      $element['_add_entity']['button'] = [
        '#type' => 'button',
        '#value' => t('Create the entity'),
        '#limit_validation_errors' => [],
        '#ajax' => [
          'callback' => [get_called_class(), 'bundleCallback'],
          'options' => ['query' => ['element_parents' => implode('/', $element_infos['#array_parents'])]],
          'wrapper' => $ajax_wrapper_id,
        ],
      ];
    } else {
      // We know that we have only one target bundle but some Entity like user
      // don't have specific bundle, in this case we use the target type.
      $target_bundle = isset($settings['handler_settings']['target_bundles']) ? array_pop($settings['handler_settings']['target_bundles']) : $settings['target_type'];
    }
    if (is_null($target_bundle) && isset($value['_add_entity']) && isset($value['_add_entity']['bundle'])) {
      $target_bundle = $value['_add_entity']['bundle'];
    }
    if (is_null($target_bundle)) {
      $input = $form_state->getUserInput();
      $parents = $element_infos['#parents'];
      $parents[] = '_add_entity';
      $parents[] = 'bundle';
      $target_bundle = NestedArray::getValue(
        $input,
        $parents);
    }
    if ($target_bundle) {
      $element['entity_components'] = [
        '#type' => 'import_schema_components',
        '#entity_type' => $settings['target_type'],
        '#entity_bundle' => $target_bundle,
        '#components' => (isset($value['entity_components']) ? $value['entity_components'] : []),
      ];
    }

    return $element;
  }

  // ---------------------------------------------------------------------------
  // Ajax callbacks.

  /**
   * Implements callback for Ajax event to add a bundle.
   *
   * @param array $form
   *   From render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current state of form.
   *
   * @return array
   */
  public static function bundleCallback(array &$form, FormStateInterface $form_state, Request $request) {
    $form_parents = explode('/', $request->query->get('element_parents'));
    $element = NestedArray::getValue($form, $form_parents);
    return $element;
  }

}
