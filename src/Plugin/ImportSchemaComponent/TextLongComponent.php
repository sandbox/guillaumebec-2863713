<?php

namespace Drupal\html2entity\Plugin\ImportSchemaComponent;

/**
 * Plugin implementation of the 'text_long' component.
 *
 * @ImportSchemaComponent(
 *   id = "text_long",
 *   label = @Translation("Text (formatted, long) component for ImportSchema field."),
 *   field_types = {},
 * )
 */
class TextLongComponent extends TextComponentBase {}
