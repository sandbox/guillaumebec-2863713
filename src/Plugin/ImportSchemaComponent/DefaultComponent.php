<?php

namespace Drupal\html2entity\Plugin\ImportSchemaComponent;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\html2entity\Plugin\ImportSchemaComponentBase;

/**
 * Plugin implementation of the 'default_component' component.
 *
 * @ImportSchemaComponent(
 *   id = "default_component",
 *   label = @Translation("Default component for ImportSchema field."),
 *   field_types = {},
 * )
 */
class DefaultComponent extends ImportSchemaComponentBase {

  /**
   * {@inheritdoc}
   */
  protected function getElementProperty(FieldDefinitionInterface $definition) {
    $storage_definition = $definition->getFieldStorageDefinition();
    $schema_columns = $storage_definition->getColumns();
    array_walk($schema_columns, function (&$value, $key) use ($storage_definition) {
      $value = $storage_definition->getPropertyDefinition($key)->getLabel();
    });
    return $schema_columns;
  }

}
