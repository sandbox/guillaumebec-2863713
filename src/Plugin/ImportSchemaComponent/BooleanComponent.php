<?php

namespace Drupal\html2entity\Plugin\ImportSchemaComponent;

use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Plugin implementation of the 'boolean' component.
 *
 * @ImportSchemaComponent(
 *   id = "boolean",
 *   label = @Translation("Boolean component for ImportSchema field."),
 *   field_types = {},
 * )
 */
class BooleanComponent extends DefaultComponent {

  /**
   * {@inheritdoc}
   */
  public function getMigrationProcess($field_name, FieldDefinitionInterface $field_definition) {
    $process = [];
    foreach ($this->getElementProperty($field_definition) as $property => $label) {
      $process[$field_name . '/' . $property] = [
        'plugin' => 'extract',
        'source' => $field_name . '_' . $property,
        'default' => 0,
        'index' => [0],
      ];
    }
    return $process;
  }

}
