<?php

namespace Drupal\html2entity\Plugin\ImportSchemaComponent;

/**
 * Plugin implementation of the 'text' component.
 *
 * @ImportSchemaComponent(
 *   id = "text",
 *   label = @Translation("Text (formatted) component for ImportSchema field."),
 *   field_types = {},
 * )
 */
class TextComponent extends TextComponentBase {}
