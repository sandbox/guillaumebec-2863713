<?php

namespace Drupal\html2entity\Plugin\ImportSchemaComponent;

use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Plugin implementation of the 'created' component.
 *
 * @ImportSchemaComponent(
 *   id = "created",
 *   label = @Translation("Created date component for ImportSchema field."),
 *   field_types = {},
 * )
 */
class CreatedComponent extends DefaultComponent {

  /**
   * {@inheritdoc}
   */
  public function getMigrationProcess($field_name, FieldDefinitionInterface $field_definition) {
    $process = [];
    foreach ($this->getElementProperty($field_definition) as $property => $label) {
      $process[$field_name . '/' . $property][] = [
        'plugin' => 'skip_on_empty',
        'method' => 'process',
        'source' => $field_name . '_' . $property,
      ];
      $process[$field_name . '/' . $property][] = [
        'plugin' => 'extract',
        'index' => [0],
      ];
      $process[$field_name . '/' . $property][] = [
        'plugin' => 'strtotime',
      ];
    }
    return $process;
  }

}
