<?php

namespace Drupal\html2entity\Plugin\ImportSchemaComponent;

use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Base plugin implementation of the formatted text components.
 */
abstract class TextComponentBase extends DefaultComponent {

  /**
   * {@inheritdoc}
   */
  public function getMigrationProcess($field_name, FieldDefinitionInterface $field_definition) {
    $process = parent::getMigrationProcess($field_name, $field_definition);

    $process[$field_name . '/value'][] = [
      'plugin' => 'html2entity_import_images',
    ];

    return $process;
  }

}
