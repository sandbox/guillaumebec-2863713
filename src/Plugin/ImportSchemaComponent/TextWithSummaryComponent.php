<?php

namespace Drupal\html2entity\Plugin\ImportSchemaComponent;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Plugin implementation of the 'text_with_summary' component.
 *
 * @ImportSchemaComponent(
 *   id = "text_with_summary",
 *   label = @Translation("Text (formatted, long, with summary) component for ImportSchema field."),
 *   field_types = {},
 * )
 */
class TextWithSummaryComponent extends TextComponentBase {

  /**
   * {@inheritdoc}
   */
  public function getMigrationProcess($field_name, FieldDefinitionInterface $field_definition) {
    $process = parent::getMigrationProcess($field_name, $field_definition);

    $process[$field_name . '/summary'][] = [
      'plugin' => 'html2entity_import_images',
    ];

    return $process;
  }

}
