<?php

namespace Drupal\html2entity\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\html2entity\ImportStateManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @QueueWorker(
 *   id = "import_state_next_step",
 *   title = @Translation("ImportState next step"),
 *   cron = {"time" = 60}
 * )
 */
class ImportStateNextStep extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The import state manager.
   *
   * @var \Drupal\html2entity\ImportStateManagerInterface
   */
  protected $importStateManager;

  /**
   * Creates a new NodePublishBase object.
   *
   * @param \Drupal\html2entity\ImportStateManagerInterface $import_state_manager
   *   The node storage.
   */
  public function __construct(ImportStateManagerInterface $import_state_manager) {
    $this->importStateManager = $import_state_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('html2entity.importstatemanager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $this->importStateManager->nextStep($data);
  }

}
