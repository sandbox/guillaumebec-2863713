<?php

namespace Drupal\html2entity;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of Import state entities.
 *
 * @ingroup html2entity
 */
class ImportStateListBuilder extends EntityListBuilder {

  use LinkGeneratorTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $bundleInfo;

  /**
   * Constructs a new EntityListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The Entity type manager service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundleInfo
   *   The bundle info service.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, EntityTypeManagerInterface $entityTypeManager, EntityTypeBundleInfoInterface $bundleInfo) {
    parent::__construct($entity_type, $storage);
    $this->entityTypeManager = $entityTypeManager;
    $this->bundleInfo = $bundleInfo;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity.manager')->getStorage($entity_type->id()),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['url'] = $this->t('Url');
    $header['target_entity_bundle'] = $this->t('Target Entity');
    $header['state'] = $this->t('State');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\html2entity\Entity\ImportState */
    $row['id'] = $this->l(
      $entity->id(),
      new Url(
        'entity.import_state.edit_form', array(
          'import_state' => $entity->id(),
        )
      )
    );
    $row['url'] = $this->l(
      $entity->label(),
      Url::fromUri($entity->label())
    );

    $entityTypeDefinitions = $this->entityTypeManager->getDefinitions();
    $bundleInfos = $this->bundleInfo->getAllBundleInfo();

    $row['target_entity_bundle'] = new FormattableMarkup('@entity_type (@bundle)', [
      '@entity_type' => $entityTypeDefinitions[$entity->getTargetEntityType()]->getLabel(),
      '@bundle' => $bundleInfos[$entity->getTargetEntityType()][$entity->getTargetBundle()]['label'],
    ]);

    // @TODO Is there a better way to get this ?
    $state_label = $entity->getFieldDefinition('state')->getItemDefinition()->getSettings()['allowed_values'];
    $row['state'] = $state_label[$entity->getState()];

    return $row + parent::buildRow($entity);
  }

}
