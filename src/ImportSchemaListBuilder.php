<?php

/**
 * @file
 * Contains \Drupal\html2entity\ImportSchemaListBuilder.
 */

namespace Drupal\html2entity;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a listing of Import schema entities.
 */
class ImportSchemaListBuilder extends ConfigEntityListBuilder {

  /**
   * All entity types.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface[]
   */
  protected $entityTypes;

  /**
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $bundleInfo;


  /**
   * Constructs a new EntityDisplayModeListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Entity\EntityTypeInterface[] $entity_types
   *   List of all entity types.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundleInfo
   *   The bundle info service.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, array $entity_types, EntityTypeBundleInfoInterface $bundleInfo) {
    parent::__construct($entity_type, $storage);

    $this->entityTypes = $entity_types;
    $this->bundleInfo = $bundleInfo;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $entity_manager = $container->get('entity.manager');
    return new static(
      $entity_type,
      $entity_manager->getStorage($entity_type->id()),
      $entity_manager->getDefinitions(),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Import schema');
    $header['target_bundle'] = $this->t('Target Bundle');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $bundleInfos = $this->bundleInfo->getAllBundleInfo();
    $row['target_bundle'] = $bundleInfos[$entity->getTargetType()][$entity->getTargetBundle()]['label'];

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function load() {
    $entities = array();
    foreach (parent::load() as $entity) {
      $entities[$entity->getTargetType()][] = $entity;
    }
    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    foreach ($this->load() as $entity_type => $entities) {
      if (!isset($this->entityTypes[$entity_type])) {
        continue;
      }

      // Filter entities.
      if (!$this->isValidEntity($entity_type)) {
        continue;
      }

      $table = array(
        '#prefix' => '<h2>' . $this->entityTypes[$entity_type]->getLabel() . '</h2>',
        '#type' => 'table',
        '#header' => $this->buildHeader(),
        '#rows' => array(),
      );
      foreach ($entities as $entity) {
        if ($row = $this->buildRow($entity)) {
          $table['#rows'][$entity->id()] = $row;
        }
      }

      // Move content at the top.
      if ($entity_type == 'node') {
        $table['#weight'] = -10;
      }

      $table['#rows']['_add_new'][] = array(
        'data' => array(
          '#type' => 'link',
          '#url' => Url::fromRoute('entity.import_schema.add_form', ['entity_type_id' => $entity_type]),
          '#title' => $this->t('Add new %label @entity-type', array('%label' => $this->entityTypes[$entity_type]->getLabel(), '@entity-type' => $this->entityType->getLowercaseLabel())),
        ),
        'colspan' => count($table['#header']),
      );
      $build[$entity_type] = $table;
    }
    if (empty($build)) {
      $build['table'] = array(
        '#type' => 'table',
        '#header' => $this->buildHeader(),
        '#title' => $this->getTitle(),
        '#rows' => array(),
        '#empty' => $this->t('There is no @label yet.', array('@label' => $this->entityType->getLabel())),
        '#cache' => [
          'contexts' => $this->entityType->getListCacheContexts(),
          'tags' => $this->entityType->getListCacheTags(),
        ],
      );
    }
    return $build;
  }

  /**
   * Filters entities based on their view builder handlers.
   *
   * @param $entity_type
   *   The entity type of the entity that needs to be validated.
   *
   * @return bool
   *   TRUE if the entity has the correct view builder handler, FALSE if the
   *   entity doesn't have the correct view builder handler.
   */
  protected function isValidEntity($entity_type) {
    return $this->entityTypes[$entity_type]->isSubclassOf('\Drupal\Core\Entity\FieldableEntityInterface');
  }

}
