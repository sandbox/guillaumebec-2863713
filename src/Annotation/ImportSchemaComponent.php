<?php

namespace Drupal\html2entity\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Import schema component item annotation object.
 *
 * @see \Drupal\html2entity\Plugin\ImportSchemaComponentManager
 * @see plugin_api
 *
 * @Annotation
 */
class ImportSchemaComponent extends Plugin {

  /**
   * The plugin ID.
   * Must be build using the same name as the ID of FieldType annotation. So if
   * you want to create a plugin for the UriItem fieldType only, your plugin ID
   * must be : uri.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * An array of field types the plugin supports.
   *
   * @var array
   */
  public $field_types = array();
}
