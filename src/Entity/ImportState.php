<?php

/**
 * @file
 * Contains \Drupal\html2entity\Entity\ImportState.
 */

namespace Drupal\html2entity\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Import state entity.
 *
 * @ingroup html2entity
 *
 * @ContentEntityType(
 *   id = "import_state",
 *   label = @Translation("Import state"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\html2entity\ImportStateListBuilder",
 *     "views_data" = "Drupal\html2entity\Entity\ImportStateViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\html2entity\Form\ImportStateForm",
 *       "add" = "Drupal\html2entity\Form\ImportStateForm",
 *       "edit" = "Drupal\html2entity\Form\ImportStateForm",
 *       "delete" = "Drupal\html2entity\Form\ImportStateDeleteForm",
 *     },
 *     "access" = "Drupal\html2entity\ImportStateAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\html2entity\ImportStateHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "import_state",
 *   admin_permission = "administer import state entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "url",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/import_state/{import_state}",
 *     "add-form" = "/admin/content/import_state/add",
 *     "edit-form" = "/admin/content/import_state/{import_state}/edit",
 *     "delete-form" = "/admin/content/import_state/{import_state}/delete",
 *     "collection" = "/admin/content/import_state",
 *   },
 *   field_ui_base_route = "import_state.settings"
 * )
 */
class ImportState extends ContentEntityBase implements ImportStateInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetEntityType() {
    return $this->get('target_entity_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTargetEntityType($target_entity_type) {
    $this->set('target_entity_type', $target_entity_type);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetBundle() {
    return $this->get('target_entity_bundle')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTargetBundle($target_bundle) {
    $this->set('target_entity_bundle', $target_bundle);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getState() {
    return $this->get('state')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setState($state) {
    $this->set('state', $state);
    return $this;
  }

  /**
   * Gets the unserialized data array.
   *
   * @return array
   *   The unserialized data. Can contain raw HTML, crawled data and error
   *   messages.
   */
  protected function getData() {
    return unserialize($this->get('data')->value);
  }

  /**
   * {@inheritdoc}
   */
  public function getDownloadedHtml() {
    $data = $this->getData();
    return $data['_raw'];
  }

  /**
   * {@inheritdoc}
   */
  public function getCrawledData() {
    $data = $this->getData();
    return $data['_crawled'];
  }

  /**
   * Sets the data array to be serialized.
   *
   * @param array $data
   *   The data array to serialize.
   *
   * @return \Drupal\html2entity\Entity\ImportStateInterface
   *   The called Import state entity.
   */
  protected function setData(array $data) {
    $this->set('data', serialize($data));
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setDownloadedHtml($html) {
    $data = $this->getData();
    $data['_raw'] = $html;
    $this->setData($data);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setCrawledData(array $values) {
    $data = $this->getData();
    $data['_crawled'] = $values;
    $this->setData($data);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getImportSchema() {
    return $this->get('import_schema')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setImportSchema($import_schema) {
    $this->set('import_schema', $import_schema);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityId() {
    return $this->get('entity_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setEntityId($id) {
    $this->set('entity_id', $id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setError($step, $message = '') {
    $this->setState($step . '_error');
    $data = $this->getData();
    $data['_message'] = $message;
    $this->setData($data);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getErrorMessage() {
    $data = $this->getData();
    return array_key_exists('_message', $data) ? $data['_message'] : '';
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Import state entity.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'hidden',
      ))
      ->setDisplayOptions('form', [
        'type' => 'hidden',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['target_entity_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Target entity type'))
      ->setDescription(t('The type of the entity that will be created.'))
      ->setRequired(TRUE)
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 2,
      ))
      ->setDisplayOptions('form', [
        'type' => 'hidden',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['target_entity_bundle'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Target entity bundle'))
      ->setDescription(t('The bundle of the entity that will be created.'))
      ->setRequired(TRUE)
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 3,
      ))
      ->setDisplayOptions('form', [
        'type' => 'hidden',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['url'] = BaseFieldDefinition::create('uri')
      ->setLabel(t('Url to import'))
      ->setDescription(t('The url of the page that will be imported.'))
      ->setRequired(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 1,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 1,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['import_schema'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Import schema'))
      ->setDescription(t('The import schema to use to import the content.'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'import_schema')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 0,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'options_select',
        'weight' => 0,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['data'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Data'))
      ->setDescription(t('Data used by the importer.'))
      ->setDefaultValue(serialize([]));

    $fields['state'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('State'))
      ->setDescription(t('State of the importer.'))
      ->setRequired(TRUE)
      ->setSettings([
        'allowed_values' =>[
          'created' => t('Starting process'),
          'waiting_download' => t('Waiting download of the html'),
          'download_error' => t('Error during download'),
          'waiting_crawl' => t('Waiting crawling of the html'),
          'crawl_error' => t('Error during crawling'),
          'waiting_migration' => t('Waiting migration of the data'),
          'migration_error' => t('Error during migration'),
          'finished' => t('Process finished'),
        ]
      ])
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 5,
      ))
      ->setDefaultValue('created');

    $fields['entity_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Id of the created entity'))
      ->setDescription(t('Id of the created entity.'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function isWaitingDownload() {
    return ($this->get('state')->value === 'waiting_download') || ($this->get('state')->value === 'download_error');
  }

  /**
   * {@inheritdoc}
   */
  public function isWaitingCrawl() {
    return ($this->get('state')->value === 'waiting_crawl') || ($this->get('state')->value === 'crawl_error');
  }

  /**
   * {@inheritdoc}
   */
  public function isWaitingMigration() {
    return ($this->get('state')->value === 'waiting_migration') || ($this->get('state')->value === 'migration_error');
  }

  /**
   * {@inheritdoc}
   */
  public function isFinished() {
    return ($this->get('state')->value === 'finished');
  }

  /**
   * {@inheritdoc}
   */
  public function setStateWaitingDownload() {
    $this->setState('waiting_download');
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setStateWaitingCrawl() {
    $this->setState('waiting_crawl');
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setStateWaitingMigration() {
    $this->setState('waiting_migration');
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setStateFinished() {
    $this->setState('finished');
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);
    if ($this->getState() == 'created') {
      \Drupal::service('html2entity.importstatemanager')->nextStep($this);
    }
  }
}
