<?php

/**
 * @file
 * Contains \Drupal\html2entity\Entity\ImportSchema.
 */

namespace Drupal\html2entity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Import schema entity.
 *
 * @ConfigEntityType(
 *   id = "import_schema",
 *   label = @Translation("Import schema"),
 *   handlers = {
 *     "list_builder" = "Drupal\html2entity\ImportSchemaListBuilder",
 *     "form" = {
 *       "add" = "Drupal\html2entity\Form\ImportSchemaForm",
 *       "edit" = "Drupal\html2entity\Form\ImportSchemaForm",
 *       "delete" = "Drupal\html2entity\Form\ImportSchemaDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\html2entity\ImportSchemaHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "import_schema",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/import_schema/{import_schema}",
 *     "add-form" = "/admin/structure/import_schema/add/{entity_type_id}",
 *     "edit-form" = "/admin/structure/import_schema/{import_schema}/edit",
 *     "delete-form" = "/admin/structure/import_schema/{import_schema}/delete",
 *     "collection" = "/admin/structure/import_schema"
 *   }
 * )
 */
class ImportSchema extends ConfigEntityBase implements ImportSchemaInterface {

  /**
   * The Import schema ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Import schema label.
   *
   * @var string
   */
  protected $label;

  /**
   * The entity type this import schema is used for.
   *
   * @var string
   */
  protected $targetEntityType;

  /**
   * The bundle for which the import schema is being created.
   *
   * @var string
   */
  protected $targetBundle;

  /**
   * List of component display options, keyed by field name.
   *
   * @var array
   */
  protected $components = array();

  /**
   * A list of field definitions eligible for configuration in this display.
   *
   * @var \Drupal\Core\Field\FieldDefinitionInterface[]
   */
  protected $fieldDefinitions;

  /**
   * The plugin manager used by this entity type.
   *
   * @var \Drupal\Component\Plugin\PluginManagerBase
   */
  protected $pluginManager;

  /**
   * Gets the definitions of the fields attached to the entity.
   */
  protected function getFieldDefinitions() {
    if (!isset($this->fieldDefinitions)) {
      $definitions = \Drupal::entityManager()->getFieldDefinitions($this->targetEntityType, $this->bundle);
      $this->fieldDefinitions = $definitions;
    }

    return $this->fieldDefinitions;
  }

  /**
   * Gets the field definition of a field.
   */
  protected function getFieldDefinition($field_name) {
    $definitions = $this->getFieldDefinitions();
    return isset($definitions[$field_name]) ? $definitions[$field_name] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetType() {
    return $this->targetEntityType;
  }
  /**
   * {@inheritdoc}
   */
  public function setTargetType($target_entity_type) {
    $this->targetEntityType = $target_entity_type;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetBundle() {
    return $this->targetBundle;
  }

  /**
   * {@inheritdoc}
   */
  public function setTargetBundle($bundle) {
    $this->set('targetBundle', $bundle);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getComponents() {
    return $this->components;
  }

  /**
   * {@inheritdoc}
   */
  public function getComponent($bundle) {
    return isset($this->components[$bundle]) ? $this->components[$bundle] : [];
  }

  /**
   * {@inheritdoc}
   */
  public function setComponent($components) {
    $this->set('components', $components);
    return $this;
  }

}
