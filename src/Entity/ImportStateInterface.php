<?php

namespace Drupal\html2entity\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Import state entities.
 *
 * @ingroup html2entity
 */
interface ImportStateInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the Import state creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Import state.
   */
  public function getCreatedTime();

  /**
   * Sets the Import state creation timestamp.
   *
   * @param int $timestamp
   *   The Import state creation timestamp.
   *
   * @return \Drupal\html2entity\Entity\ImportStateInterface
   *   The called Import state entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the ImportState state.
   *
   * @return string
   *   State of the Import state.
   */
  public function getState();

  /**
   * Sets the Import state state.
   *
   * @param string $state
   *   The Import state state.
   *
   * @return \Drupal\html2entity\Entity\ImportStateInterface
   *   The called Import state entity.
   */
  public function setState($state);

  /**
   * Gets the Import State target entity type.
   *
   * @return string
   *   Target entity type of the Import state.
   */
  public function getTargetEntityType();

  /**
   * Sets the Import state target entity type.
   *
   * @param string $target_entity_type
   *   The Import state target entity type.
   *
   * @return \Drupal\html2entity\Entity\ImportStateInterface
   *   The called Import state entity.
   */
  public function setTargetEntityType($target_entity_type);

  /**
   * Gets the Import State target bundle.
   *
   * @return string
   *   Target bundle of the Import state.
   */
  public function getTargetBundle();

  /**
   * Sets the Import state target entity type.
   *
   * @param string $target_bundle
   *   The Import state target bundle.
   *
   * @return \Drupal\html2entity\Entity\ImportStateInterface
   *   The called Import state entity.
   */
  public function setTargetBundle($target_bundle);

  /**
   * Gets the downloaded HTML.
   *
   * @return string
   *   The downloaded HTML.
   */
  public function getDownloadedHtml();

  /**
   * Gets the crawled data.
   *
   * @return array
   *   The crawled data, keyed according to the ImportSchema.
   */
  public function getCrawledData();

  /**
   * Sets the downloaded HTML.
   *
   * @param string $html
   *   The downloaded HTML.
   *
   * @return \Drupal\html2entity\Entity\ImportStateInterface
   *   The called Import state entity.
   */
  public function setDownloadedHtml($html);

  /**
   * Sets the crawled data.
   *
   * @param array $values
   *   The crawled data.
   *
   * @return \Drupal\html2entity\Entity\ImportStateInterface
   *   The called Import state entity.
   */
  public function setCrawledData(array $values);

  /**
   * Gets the Import Schema machine name.
   *
   * @return string
   *   Name of the Import schema machine name.
   */
  public function getImportSchema();

  /**
   * Sets the Import Schema machine name.
   *
   * @param string $import_schema
   *   The Import schema machine name.
   *
   * @return \Drupal\html2entity\Entity\ImportStateInterface
   *   The called Import state entity.
   */
  public function setImportSchema($import_schema);

  /**
   * Gets the migrated Entity Id.
   *
   * @return string
   *   The Id of the migrated Entity.
   */
  public function getEntityId();

  /**
   * Sets the migrated Entity Id.
   *
   * @param string $id
   *   The Id of the migrated Entity.
   *
   * @return \Drupal\html2entity\Entity\ImportStateInterface
   *   The called Import state entity.
   */
  public function setEntityId($id);

  /**
   * Sets the error state and an error message.
   *
   * @param string $step
   *   download, crawl or migration
   * @param string $message
   *   The error message
   *
   * @return \Drupal\html2entity\Entity\ImportStateInterface
   *   The called Import state entity.
   */
  public function setError($step, $message = '');

  /**
   * Gets the error message.
   *
   * @return string
   *   The error message.
   */
  public function getErrorMessage();

  /**
   * Test the Import state state indicator.
   *
   * @return bool
   *   TRUE if the Import state is waiting to download the url content.
   */
  public function isWaitingDownload();

  /**
   * Test the Import state state indicator.
   *
   * @return bool
   *   TRUE if the Import state is waiting to parse the data.
   */
  public function isWaitingCrawl();

  /**
   * Test the Import state state indicator.
   *
   * @return bool
   *   TRUE if the Import state is waiting to migrate the data.
   */
  public function isWaitingMigration();

  /**
   * Test the Import state state indicator.
   *
   * @return bool
   *   TRUE if the Import state as completed all tasks.
   */
  public function isFinished();

  /**
   * Sets the Import state state to waiting for download.
   *
   * @return \Drupal\html2entity\Entity\ImportStateInterface
   *   The called Import state entity.
   */
  public function setStateWaitingDownload();

  /**
   * Sets the Import state state to waiting for crawl.
   *
   * @return \Drupal\html2entity\Entity\ImportStateInterface
   *   The called Import state entity.
   */
  public function setStateWaitingCrawl();

  /**
   * Sets the Import state state to waiting for migration.
   *
   * @return \Drupal\html2entity\Entity\ImportStateInterface
   *   The called Import state entity.
   */
  public function setStateWaitingMigration();

  /**
   * Sets the Import state state to finished.
   *
   * @return \Drupal\html2entity\Entity\ImportStateInterface
   *   The called Import state entity.
   */
  public function setStateFinished();

}
