<?php

/**
 * @file
 * Contains \Drupal\html2entity\Entity\ImportSchemaInterface.
 */

namespace Drupal\html2entity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Import schema entities.
 */
interface ImportSchemaInterface extends ConfigEntityInterface {

  /**
   * Gets the target entity type.
   *
   * @return string
   */
  public function getTargetType();

  /**
   * Sets the target entity type.
   *
   * @param string $target_entity_type
   *   The target entity type.
   *
   * @return \Drupal\html2entity\Entity\ImportSchemaInterface
   *   The ImportSchema.
   */
  public function setTargetType($target_entity_type);

  /**
   * Gets the target bundle.
   *
   * @return string
   */
  public function getTargetBundle();

  /**
   * Sets the target bundle.
   *
   * @param string $bundle
   *   The target bundle.
   *
   * @return \Drupal\html2entity\Entity\ImportSchemaInterface
   *   The ImportSchema.
   */
  public function setTargetBundle($bundle);

  /**
   * Gets all components.
   *
   * @return array
   *   The components array keyed by bundle.
   */
  public function getComponents();

  /**
   * Gets all components of a given bundle.
   *
   * @param $bundle
   * @return array
   */
  public function getComponent($bundle);

  /**
   * Sets components.
   *
   * @param $components
   * @return mixed
   */
  public function setComponent($components);

}
