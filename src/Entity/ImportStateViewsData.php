<?php

namespace Drupal\html2entity\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Import state entities.
 */
class ImportStateViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
