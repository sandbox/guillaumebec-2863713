<?php

namespace Drupal\html2entity;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\html2entity\Entity\ImportStateInterface;
use Drupal\html2entity\Event\ImportStateEvents;
use Drupal\migrate\MigrateExecutable;
use Drupal\migrate\MigrateMessage;
use Drupal\migrate\Plugin\MigrationInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\html2entity\Event\ImportStateEvent;

/**
 * Class ImportStateManager.
 *
 * @package Drupal\html2entity
 */
class ImportStateManager implements ImportStateManagerInterface {

  /**
   * GuzzleHttp\Client definition.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The ImportStateNextStep queue.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * The ImportSchema manager service.
   *
   * @var \Drupal\html2entity\ImportSchemaManagerInterface
   */
  protected $importSchemaManager;

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entity_type_manager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface
   * The event dispatcher.
   */
  public function __construct(EntityTypeManager $entity_type_manager, EventDispatcherInterface $eventDispatcher) {
    $this->entity_type_manager = $entity_type_manager;
    $this->eventDispatcher = $eventDispatcher;
  }

  /**
   * @return \GuzzleHttp\Client|mixed
   */
  protected function getHttpClient() {
    if (empty($this->httpClient)) {
      $this->httpClient = \Drupal::service('http_client');
    }
    return $this->httpClient;
  }

  /**
   * @return \Drupal\Core\Queue\QueueInterface
   */
  protected function getQueue() {
    if (empty($this->queue)) {
      $this->queue = \Drupal::service('queue')->get('import_state_next_step');
    }
    return $this->queue;
  }

  /**
   * @return \Drupal\html2entity\ImportSchemaManagerInterface
   */
  protected function getImportSchemaManager() {
    if (empty($this->migrationManager)) {
      $this->importSchemaManager = \Drupal::service('html2entity.import_schema_manager');
    }
    return $this->importSchemaManager;
  }

  /**
   * Return an ImportState object.
   *
   * @param $id
   *   Id of an import state.
   * @return \Drupal\html2entity\Entity\ImportStateInterface|null
   */
  protected function getImportState($id) {
    return $this->entity_type_manager->getStorage('import_state')->load($id);
  }

  /**
   * Move an import state to its next state.
   *
   * @param ImportStateInterface|int $importState
   *   The ImportState to process or its ID.
   */
  public function nextStep($importState) {
    if (!($importState instanceof ImportStateInterface)) {
      $importState = $this->getImportState($importState);
    }
    if (empty($importState)) {
      return;
    }

    switch ($importState->getState()) {
      case 'created':
        $this->eventDispatcher->dispatch(ImportStateEvents::HTML2ENTITY_IMPORT_STATE_CREATED , new ImportStateEvent($importState));
        $importState->setState('waiting_download')->save();
        if ($importState->getState() == 'waiting_download') {
          $this->getQueue()->createItem($importState->id());
        }
        break;
      case 'waiting_download':
      case 'download_error':
        $this->eventDispatcher->dispatch(ImportStateEvents::HTML2ENTITY_IMPORT_STATE_WAITING_DOWNLOAD, new ImportStateEvent($importState));
        $this->downloadHtml($importState);
        if ($importState->getState() == 'waiting_crawl') {
          $this->getQueue()->createItem($importState->id());
        }
        break;
      case 'waiting_crawl':
      case 'crawl_error':
        $this->eventDispatcher->dispatch(ImportStateEvents::HTML2ENTITY_IMPORT_STATE_WAITING_CRAWL, new ImportStateEvent($importState));
        $this->crawlHtml($importState);
        if ($importState->getState() == 'waiting_migration') {
          $this->getQueue()->createItem($importState->id());
        }
        break;
      case 'waiting_migration':
      case 'migration_error':
        $this->eventDispatcher->dispatch(ImportStateEvents::HTML2ENTITY_IMPORT_STATE_WAITING_MIGRATION, new ImportStateEvent($importState));
        $this->migrateData($importState);
        if ($importState->getState() == 'finished') {
          $this->getQueue()->createItem($importState->id());
        }
        break;
      case 'finished':
        $this->eventDispatcher->dispatch(ImportStateEvents::HTML2ENTITY_IMPORT_STATE_FINISHED, new ImportStateEvent($importState));
        break;
      default:
        // Do nothing.
        break;
    }
  }

  /**
   * Download the html from the url field of an ImportState and save it to the
   * Entity.
   *
   * @param ImportStateInterface|int $importState
   *   The ImportState to process or its ID.
   */
  public function downloadHtml($importState) {
    if (!($importState instanceof ImportStateInterface)) {
      $importState = $this->getImportState($importState);
    }

    $url = $importState->get('url');
    if ($importState->isWaitingDownload() && !$url->isEmpty()) {
      try {
        $httpClient = $this->getHttpClient();
        $response = $httpClient->get($url->value, array('headers' => array('Accept' => 'text/plain')));
        $html = (string) $response->getBody();
        if (empty($html)) {
          throw new \Exception('The download retreived an empty result.');
        }
        $importState->setStateWaitingCrawl();
        $importState->setDownloadedHtml($html);
      }
      catch (\Exception $e) {
        $importState->setError('download', $e->getMessage());
      }
      $importState->save();
    }
  }

  /**
   * Parse the html from the data field of an ImportState based on the selector
   * filters configuration. Save the data to the entity.
   *
   * @param ImportStateInterface|int $importState
   *   The ImportState to process or its ID.
   */
  public function crawlHtml($importState) {
    if (!($importState instanceof ImportStateInterface)) {
      $importState = $this->getImportState($importState);
    }

    $importSchema_machine_name = $importState->getImportSchema();
    /** @var \Drupal\html2entity\Entity\ImportSchema $importSchema */
    $importSchema = $this->entity_type_manager->getStorage('import_schema')->load($importSchema_machine_name);
    $html = $importState->getDownloadedHtml();
    $new_data = array();
    if ($importState->isWaitingCrawl() && !empty($html)) {

      $url = $importState->get('url');
      $base_href = parse_url($url->value, PHP_URL_SCHEME) . '://' . parse_url($url->value, PHP_URL_HOST);
      $crawler = new Crawler($html, $url->value, $base_href);
      // @TODO use the getComponent($bundle) instead.
      $components = $importSchema->getComponents();
      if (!empty($components[$importState->get('target_entity_bundle')->value])) {
        foreach($components[$importState->get('target_entity_bundle')->value] as $field_name => $field_property) {
          if ($field_property) {
            foreach ($field_property as $property_name => $infos) {
              if (isset($infos['selector']) && !empty($infos['selector'])) {
                try {
                  $new_data[$field_name . '_' . $property_name] = [];
                  $selectors = explode(',', $infos['selector']);
                  foreach($selectors as $selector) {
                    if (preg_match('/^static\[(.*)\]$/', $selector, $matches)) {
                      $new_values = [$matches[1]];
                    }
                    else {
                      $new_values = $crawler->filter($selector)
                        ->each(function (Crawler $node, $i) use ($infos) {
                          switch ($infos['data_source_type']) {
                            case 'html':
                              return $node->html();
                              break;
                            case 'attr':
                              return $node->attr($infos['data_source']);
                              break;
                            default:
                              return NULL;
                          }
                        });
                    }
                    $new_data[$field_name . '_' . $property_name] = array_merge($new_data[$field_name . '_' . $property_name], $new_values);
                  }
                } catch (\InvalidArgumentException $e) {
                  $new_data[$field_name . '_' . $property_name] = '';
                }
              }
            }
          }
        }
        $importState->setState('waiting_migration');
        $importState->setCrawledData($new_data);
      }
      else {
        $importState->setError('crawl', 'Cannot find components for this bundle.');
      }
      $importState->save();
    }
  }

  /**
   * Migrate the parsed data of an ImportState using the migration defined via
   * the ImportSchema.
   *
   * @param ImportStateInterface|int $importState
   *   The ImportState to process or its ID.
   */
  public function migrateData($importState) {
    if (!($importState instanceof ImportStateInterface)) {
      $importState = $this->getImportState($importState);
    }

    if ($importState->isWaitingMigration()) {
      $importSchema = $importState->getImportSchema();
      $configuration = [];

      // Get data from the ImportState.
      $configuration['source']['constants']['source_base_url'] = substr($importState->url->value, 0, strpos($importState->url->value, '/', 10));
      $configuration['source']['data_rows'][0] = $importState->getCrawledData() + [
          'created' => \Drupal::time()->getRequestTime(),
          'id' => $importState->id(),
          'langcode' => \Drupal::languageManager()->getDefaultLanguage()->getId(),
          'type' => $importState->getTargetBundle(),
          'uid' => $importState->getOwnerId(),
        ];

      // Load and run the migration.
      /** @var \Drupal\migrate\Plugin\Migration $migration */
      $migration = $this->getImportSchemaManager()->generateMigration($importSchema, $configuration);
      if ($migration) {
        $executable = new MigrateExecutable($migration, new MigrateMessage());
        try {
          /** @var \Drupal\migrate\Plugin\MigrateIdMapInterface $idMap */
          $idMap = $migration->getIdMap();

          // Force update mode to ensure the migration can be run twice.
          $idMap->prepareUpdate();

          // Run the migration.
          $executable->import();

          // Store the new entity ID in the ImportState.
          $destinationIds = $idMap->lookupDestinationIds([$importState->id()]);
          if (empty($destinationIds)) {
            throw new \Exception('Unable to retrieve the new entity id.');
          }
          $importState->setEntityId($destinationIds[0][0]);

          // Assign author to the new entity.
          $created_entity_id = $importState->getEntityId();
          $created_entity_type = $importState->getTargetEntityType();
          $created_entity = $this->entity_type_manager->getStorage($created_entity_type)->load($created_entity_id);
          if (is_subclass_of($created_entity, '\Drupal\user\EntityOwnerInterface')) {
            $created_entity->setOwnerId($importState->getOwnerId());
            $created_entity->save();
          }

          $importState->setState('finished');
        }
        catch (\Exception $e) {
          $migration->setStatus(MigrationInterface::STATUS_IDLE);
          $importState->setError('migration', $e->getMessage());
        }
      }
      else {
        $importState->setError('migration', 'Migration not found.');
      }
      $importState->save();
    }
  }
}
