<?php

namespace Drupal\html2entity;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Import state entity.
 *
 * @see \Drupal\html2entity\Entity\ImportState.
 */
class ImportStateAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\html2entity\Entity\ImportStateInterface $entity */
    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view published import state entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit import state entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete import state entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add import state entities');
  }

}
