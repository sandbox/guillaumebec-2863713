<?php

namespace Drupal\html2entity\Event;

use Drupal\html2entity\Entity\ImportStateInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Defines the import_state event.
 *
 */
class ImportStateEvent extends Event {

  /**
   * The import state.
   *
   * @var \Drupal\html2entity\Entity\ImportStateInterface
   */
  protected $importState;

  /**
   * Constructs a new ImportStateEvent.
   *
   * @param \Drupal\html2entity\Entity\ImportStateInterface
   *   The import state.
   */
  public function __construct(ImportStateInterface $importState) {
    $this->importState = $importState;
  }

  /**
   * Gets the import state.
   *
   * @return \Drupal\html2entity\Entity\ImportStateInterface
   *   Gets the import state.
   */
  public function getImportState() {
    return $this->importState;
  }

}
