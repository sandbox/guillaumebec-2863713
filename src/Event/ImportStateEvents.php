<?php

namespace Drupal\html2entity\Event;

final class ImportStateEvents {

  /**
   * Name of the event fired when a import state is created.
   *
   * Fired before the import state is set to waiting download and queued.
   *
   * @Event
   *
   * @see \Drupal\html2entity\Event\ImportStateEvent
   */
  const HTML2ENTITY_IMPORT_STATE_CREATED = 'html2entity.import_state.created';

  /**
   * Name of the event fired before download is performed.
   *
   * @Event
   *
   * @see \Drupal\html2entity\Event\ImportStateEvent
   */
  const HTML2ENTITY_IMPORT_STATE_WAITING_DOWNLOAD = 'html2entity.import_state.waiting_download';

  /**
   * Name of the event fired when there is download error and before download is performed.
   *
   * @Event
   *
   * @see \Drupal\html2entity\Event\ImportStateEvent
   */
  const HTML2ENTITY_IMPORT_STATE_DOWNLOAD_ERROR = 'html2entity.import_state.download_error';

  /**
   * Name of the event fired before html is crawled.
   *
   * @Event
   *
   * @see \Drupal\html2entity\Event\ImportStateEvent
   */
  const HTML2ENTITY_IMPORT_STATE_WAITING_CRAWL = 'html2entity.import_state.waiting_crawl';

  /**
   * Name of the event fired when there is crawl error and before crawling is performed.
   *
   * @Event
   *
   * @see \Drupal\html2entity\Event\ImportStateEvent
   */
  const HTML2ENTITY_IMPORT_STATE_CRAWL_ERROR = 'html2entity.import_state.crawl_error';

  /**
   * Name of the event fired before html is migrated.
   *
   * @Event
   *
   * @see \Drupal\html2entity\Event\ImportStateEvent
   */
  const HTML2ENTITY_IMPORT_STATE_WAITING_MIGRATION = 'html2entity.import_state.waiting_migration';

  /**
   * Name of the event fired when there is migration error and before migration is performed.
   *
   * @Event
   *
   * @see \Drupal\html2entity\Event\ImportStateEvent
   */
  const HTML2ENTITY_IMPORT_STATE_MIGRATION_ERROR = 'html2entity.import_state.migration_error';

  /**
   * Name of the event fired when import state migration is finished.
   *
   * @Event
   *
   * @see \Drupal\html2entity\Event\ImportStateEvent
   */
  const HTML2ENTITY_IMPORT_STATE_FINISHED = 'html2entity.import_state.finished';

}
