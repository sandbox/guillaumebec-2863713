<?php

namespace Drupal\html2entity;

use Drupal\html2entity\Entity\ImportStateInterface;

interface ImportStateManagerInterface {

  /**
   * Move an import state to its next state.
   *
   * @param ImportStateInterface|int $importState
   *   The ImportState to process or its ID.
   */
  public function nextStep($importState);

  /**
   * Download the html from the url field of an ImportState and save it to the
   * Entity.
   *
   * @param ImportStateInterface|int $importState
   *   The ImportState to process or its ID.
   * @return bool
   */
  public function downloadHtml($importState);

  /**
   * Parse the html from the data field of an ImportState based on the selector
   * filters configuration. Save the data to the entity.
   *
   * @param ImportStateInterface|int $importState
   *   The ImportState to process or its ID.
   * @return void
   */
  public function crawlHtml($importState);

  /**
   * Migrate the parsed data of an ImportState using the migration defined via
   * the ImportSchema.
   *
   * @param ImportStateInterface|int $importState
   *   The ImportState to process or its ID.
   * @return void
   */
  public function migrateData($importState);

}
