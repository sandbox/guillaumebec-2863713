<?php

/**
 * @file
 * Contains \Drupal\html2entity\Form\ImportSchemaForm.
 */

namespace Drupal\html2entity\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\html2entity\Plugin\ImportSchemaComponentManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ImportSchemaForm.
 *
 * @package Drupal\html2entity\Form
 */
class ImportSchemaForm extends EntityForm {

  /**
   * The entity type for which the display mode is being created.
   *
   * @var string
   */
  protected $targetEntityTypeId;

  /**
   * The entity query factory.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  protected $queryFactory;

  /**
   * The entity type definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected $entityType;

  /**
   * The entity manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $entityManager;

  /**
   * The ImportSchema Component manager.
   * @var \Drupal\html2entity\Plugin\ImportSchemaComponentManager
   */
  protected $componentPluginManager;

  /**
   * Constructs a new ImportSchemaForm.
   *
   * @param \Drupal\Core\Entity\Query\QueryFactory $query_factory
   *   The entity query factory.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   * @param \Drupal\html2entity\Plugin\ImportSchemaComponentManager $import_schema_component_manager
   *   The ImportSchema Component manager.
   */
  public function __construct(QueryFactory $query_factory, EntityManagerInterface $entity_manager, ImportSchemaComponentManager $import_schema_component_manager) {
    $this->queryFactory = $query_factory;
    $this->entityManager = $entity_manager;
    $this->componentPluginManager = $import_schema_component_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.query'),
      $container->get('entity.manager'),
      $container->get('plugin.manager.import_schema_component')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function init(FormStateInterface $form_state) {
    parent::init($form_state);
    $this->entityType = $this->entityManager->getDefinition($this->entity->getEntityTypeId());
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\html2entity\Entity\ImportSchemaInterface $entity */
    $entity = $this->entity;

    $targeted_entity_type = $entity->getTargetType();
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#maxlength' => 100,
      '#default_value' => $entity->label(),
      '#description' => $this->t("Name of the Import Schema, visible by users."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#description' => $this->t('A unique machine-readable name. Can only contain lowercase letters, numbers, and underscores.'),
      '#default_value' => $entity->id(),
      '#field_prefix' => $this->entity->isNew() ? $targeted_entity_type . '.' : '',
      '#machine_name' => [
        'exists' => '\Drupal\html2entity\Entity\ImportSchema::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $bundles = $this->entityManager->getBundleInfo($targeted_entity_type);
    $bundle_options = [];
    foreach ($bundles as $bundle_name => $bundle_info) {
      $bundle_options[$bundle_name] = $bundle_info['label'];
    }
    natsort($bundle_options);

    $target_bundle = $entity->getTargetBundle();

    $input = $form_state->getUserInput();
    if (!$target_bundle && !empty($input)) {
      $target_bundle = $input['targetBundle'];
    }

    $form['targetBundle'] = array(
      '#type' => 'select',
      '#title' => $this->t('Bundle'),
      '#options' => $bundle_options,
      '#default_value' => $target_bundle,
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::bundleCallback',
        'event' => 'change',
        'wrapper' => 'bundle-wrapper',
      ],
    );

    $form['components'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'bundle-wrapper'],
      '#tree' => TRUE,
    ];
    $form['components'][$target_bundle] = [
      '#type' => 'import_schema_components',
      '#entity_type' => $targeted_entity_type,
      '#entity_bundle' => $target_bundle,
      '#components' => $entity->getComponent($target_bundle),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $entity_type_id = NULL) {
    $entity = $this->entity;
    if (!is_null($entity_type_id)) {
      $entity->setTargetType($entity_type_id);
    }

    $form = parent::buildForm($form, $form_state);
    // Change replace_pattern to avoid undesired dots.
    $form['id']['#machine_name']['replace_pattern'] = '[^a-z0-9_]+';

    $definition = $this->entityManager->getDefinition($this->entity->getTargetType());
    $form['#title'] = $this->t('Add new %label @entity-type', array('%label' => $definition->getLabel(), '@entity-type' => $this->entityType->getLowercaseLabel()));
    return $form;
  }

  /**
   * Implements callback for Ajax event to add a bundle.
   *
   * @param array $form
   *   From render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current state of form.
   *
   * @return array
   */
  public function bundleCallback(array &$form, FormStateInterface $form_state) {
    return $form['components'];
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $import_schema = $this->entity;
    $status = $import_schema->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Import schema.', [
          '%label' => $import_schema->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Import schema.', [
          '%label' => $import_schema->label(),
        ]));
    }
    $form_state->setRedirectUrl($import_schema->toUrl('collection'));
  }

}
