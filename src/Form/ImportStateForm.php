<?php

namespace Drupal\html2entity\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\ContentEntityFormInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Import state edit forms.
 *
 * @ingroup html2entity
 */
class ImportStateForm extends ContentEntityForm implements ContentEntityFormInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $bundleInfo;

  /**
   * Constructs a ContentEntityForm object.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundleInfo
   *   The bundle info service.
   */
  public function __construct(EntityManagerInterface $entity_manager, EntityTypeBundleInfoInterface $bundleInfo) {
    parent::__construct($entity_manager);
    $this->bundleInfo = $bundleInfo;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\html2entity\Entity\ImportState $entity */
    $entity = &$this->entity;

    // Set target entity type and bundle based on the selected import schema.
    $import_schema = $entity->getImportSchema();
    /** @var \Drupal\html2entity\Entity\ImportSchema $importSchema */
    $importSchema = $this->entityTypeManager->getStorage('import_schema')->load($import_schema);
    $entity->setTargetEntityType($importSchema->getTargetType());
    $entity->setTargetBundle($importSchema->getTargetBundle());

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Import state.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Import state.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.import_state.canonical', ['import_state' => $entity->id()]);
  }

  /**
   * Ajax callback to refresh the target bundle field when target entity type is
   * selected.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @return array
   *   The render array of the field to replace.
   */
  public static function ajaxRefreshBundles($form, FormStateInterface $form_state) {
    return $form['target_entity_bundle']['widget'][0]['value'];
  }

}
