<?php

namespace Drupal\html2entity\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Import state entities.
 *
 * @ingroup html2entity
 */
class ImportStateDeleteForm extends ContentEntityDeleteForm {


}
