<?php

namespace Drupal\html2entity;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\html2entity\Plugin\ImportSchemaComponentManager;
use Drupal\migrate\Plugin\MigrationPluginManager;

/**
 * Class ImportSchemaManager.
 *
 * @package Drupal\html2entity
 */
class ImportSchemaManager implements ImportSchemaManagerInterface {

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $importSchemaStorage;

  /**
   * @var \Drupal\migrate\Plugin\MigrationPluginManager
   */
  protected $migrationManager;

  /**
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * @var \Drupal\html2entity\Plugin\ImportSchemaComponentManager
   */
  protected $importSchemaComponentManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   * @param \Drupal\migrate\Plugin\MigrationPluginManager $migration_manager
   */
  public function __construct(EntityTypeManager $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, MigrationPluginManager $migration_manager, ImportSchemaComponentManager $import_schema_component_manager) {
    $this->importSchemaStorage = $entity_type_manager->getStorage('import_schema');
    $this->entityFieldManager = $entity_field_manager;
    $this->migrationManager = $migration_manager;
    $this->importSchemaComponentManager = $import_schema_component_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function generateMigration($importSchema, array $configuration = []) {
    if (is_scalar($importSchema)) {
      $importSchema = $this->importSchemaStorage->load($importSchema);
    }

    /** @var \Drupal\migrate\Plugin\Migration $migration */
    $migration = $this->migrationManager->createInstance('html2entity_root', $configuration);

    $bundle_fields = $this->entityFieldManager->getFieldDefinitions($importSchema->getTargetType(), $importSchema->getTargetBundle());
    foreach ($importSchema->getComponent($importSchema->getTargetBundle()) as $field_name => $settings) {
      $field_definition = $bundle_fields[$field_name];
      $field_type = $field_definition->getType();
      $component_plugin = $this->importSchemaComponentManager->createInstance($field_type);
      $migration_process = $component_plugin->getMigrationProcess($field_name, $field_definition);
      foreach($migration_process as $destination => $process) {
        $migration->setProcessOfProperty($destination, $process);
      }
    }

    return $migration;
  }

}
