<?php

namespace Drupal\html2entity;

use Drupal\html2entity\Entity\ImportSchemaInterface;

/**
 * Interface ImportSchemaManagerInterface.
 *
 * @package Drupal\html2entity
 */
interface ImportSchemaManagerInterface {

  /**
   * Generate a migration out of an import schema entity.
   *
   * @param \Drupal\html2entity\Entity\ImportSchemaInterface|string $importSchema
   *   The import schema or its ID.
   * @param array $configuration
   *   Configuration to be injected in the migration.
   *
   * @return \Drupal\migrate\Plugin\Migration
   *   The generated migration.
   */
  public function generateMigration($importSchema, array $configuration = []);

}
