<?php

/**
 * @file
 * Contains import_state.page.inc.
 *
 * Page callback for Import state entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Import state templates.
 *
 * Default template: import_state.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_import_state(array &$variables) {
  // Fetch ImportState Entity Object.
  $import_state = $variables['elements']['#import_state'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
